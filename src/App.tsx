import React, { useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import { connect } from 'react-redux';
import { IEmployee } from './models/employee';
import { AppState } from './state/store';
import { fetchEmployeesAsync, Dispatch } from './state/actions';


const App: React.FC<DispatchProps & StateProps> = ({ fetchEmployees, isFetching, employees }) => {

  useEffect(() => {
    console.log("EMPLOYEES", employees)
  }, [employees])

  useEffect(() => {
    console.log("IS FETCHING", isFetching)
  }, [isFetching])

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <input type="button" value="Fetch users" onClick={() => fetchEmployees()} />
      </header>
    </div>
  );
}

interface DispatchProps {
  fetchEmployees: () => void
 }

 interface StateProps {
  isFetching: boolean
  employees: IEmployee[]
 }
 
 const mapStateToProps = (state: AppState): StateProps => {
  return {
    employees: state.employeesReducer.employees,
    isFetching: state.employeesReducer.isFetching
  }
 }
 
 const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => {
  return {
    fetchEmployees: () => dispatch(fetchEmployeesAsync())
  }
 }
 

export default connect(mapStateToProps, mapDispatchToProps)(App)

