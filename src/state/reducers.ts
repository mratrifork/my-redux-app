import { IEmployee } from "../models/employee"

import { getType } from "typesafe-actions"
import {
  fetchEmployeesActions,
  employeeActionTypes,
  removeEmployeeActions
} from "./actions"

export interface IEmployeeReducerState {
  isFetching: boolean
  employees: IEmployee[]
}

const initialState: IEmployeeReducerState = {
  employees: [],
  isFetching: false
}

export const employeesReducer = (
  state = initialState,
  action: employeeActionTypes
): IEmployeeReducerState => {
  switch (action.type) {
    case getType(fetchEmployeesActions.request):
      return { ...state, isFetching: true }
    case getType(fetchEmployeesActions.success):
      return { ...state, isFetching: false, employees: action.payload }
    case getType(fetchEmployeesActions.failure):
      return { ...state, isFetching: false }
    case getType(removeEmployeeActions.failure):
      return { ...state, isFetching: false }
    default:
      return state
  }
}
