import { createAsyncAction, ActionType } from "typesafe-actions"
import { IEmployee } from "../models/employee"

import { AnyAction } from "redux"
import { ThunkAction, ThunkDispatch } from "redux-thunk"
import { AppState } from "./store";

export type Action = ThunkAction<void, AppState, null, AnyAction>
export type Dispatch = ThunkDispatch<AppState, null, AnyAction>

export const fetchEmployeesActions = createAsyncAction(
 "FETCH_EMPLOYEES_REQUEST",
 "FETCH_EMPLOYEES_SUCCESS",
 "FETCH_EMPLOYEES_FAILURE"
)<void, IEmployee[], Error>()

export const removeEmployeeActions = createAsyncAction(
    "REMOVE_EMPLOYEE_REQUEST",
    "REMOVE_EMPLOYEE_SUCCESS",
    "REMOVE_EMPLOYEE_FAILURE"
)<void, string, Error>()

export type fetchEmployeesTypes = ActionType<typeof fetchEmployeesActions>
export type removeEmployeeTypes = ActionType<typeof removeEmployeeActions>

export type employeeActionTypes = fetchEmployeesTypes | removeEmployeeTypes

export const fetchEmployeesAsync = (): Action => async dispatch => {
    dispatch(fetchEmployeesActions.request())
   
    await setTimeout(() => {
      dispatch(
        fetchEmployeesActions.success([
          { id: 1, name: "John Doe" },
          { id: 2, name: "Jane Doe" }
        ])
      )
    }, 5000)
   }
   